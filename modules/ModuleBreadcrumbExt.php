<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-extended-navigation
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */

namespace Navigation;


/**
 * Front end module "breadcrumb".
 *
 * @author Leo Feyer <https://github.com/leofeyer>
 */
class ModuleBreadcrumbExt extends \ModuleBreadcrumb
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_breadcrumb';


	/**
	 * Display a wildcard in the back end
	 *
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			/** @var \BackendTemplate|object $objTemplate */
			$objTemplate = new \BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### ' . $GLOBALS['TL_LANG']['FMD'][$this->type][0] . ' ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}

		return parent::generate();
	}


	/**
	 * Generate the module
	 */
	protected function compile()
	{
		/** @var \PageModel $objPage */
		global $objPage;

		$type = null;
		$pageId = $objPage->id;
		$pages = array($objPage->row());
		$items = array();

		// Get all pages up to the root page
		$objPages = \PageModel::findParentsById($objPage->pid);

		if ($objPages !== null)
		{
			while ($pageId > 0 && $type != 'root' && $objPages->next())
			{
				$type = $objPages->type;
				$pageId = $objPages->pid;
				$pages[] = $objPages->row();
			}
		}

		// Get the first active regular page and display it instead of the root page
		if ($type == 'root')
		{
			$objFirstPage = \PageModel::findFirstPublishedByPid($objPages->id);

			$items[] = array
			(
				'isRoot'   => true,
				'isActive' => false,
				'href'     => (($objFirstPage !== null) ? $this->generateFrontendUrl($objFirstPage->row()) : \Environment::get('base')),
				'title'    => specialchars($objPages->pageTitle ?: $objPages->title, true),
				'link'     => $objPages->title,
				'data'     => $objFirstPage->row(),
				'class'    => ''
			);

			array_pop($pages);
		}

		//Navigation Extended
    $arrHidePages = deserialize($this->hidePages);

		// Build the breadcrumb menu
		for ($i=(count($pages)-1); $i>0; $i--)
		{

			if($arrHidePages)
			{
				if(in_array($pages[$i]['id'], $arrHidePages))
				{
					continue;
				}
			}

			if (($pages[$i]['hide'] && !$this->showHidden) || (!$pages[$i]['published'] && !BE_USER_LOGGED_IN))
			{
				continue;
			}

			// Get href
			switch ($pages[$i]['type'])
			{
				case 'redirect':
					$href = $pages[$i]['url'];

					if (strncasecmp($href, 'mailto:', 7) === 0)
					{
						$href = \String::encodeEmail($href);
					}
					break;

				case 'forward':
					$objNext = \PageModel::findPublishedById($pages[$i]['jumpTo']);

					if ($objNext !== null)
					{
						$href = $this->generateFrontendUrl($objNext->row());
						break;
					}
					// DO NOT ADD A break; STATEMENT

				default:
					$href = $this->generateFrontendUrl($pages[$i]);
					break;
			}

			$items[] = array
			(
				'isRoot'   => false,
				'isActive' => false,
				'href'     => $href,
				'title'    => specialchars($pages[$i]['pageTitle'] ?: $pages[$i]['title'], true),
				'link'     => $pages[$i]['title'],
				'data'     => $pages[$i],
				'class'    => $pages[$i]['cssClass']
			);
		}

		// Active article
		if (isset($_GET['articles']))
		{
			$items[] = array
			(
				'isRoot'   => false,
				'isActive' => false,
				'href'     => $this->generateFrontendUrl($pages[0]),
				'title'    => specialchars($pages[0]['pageTitle'] ?: $pages[0]['title'], true),
				'link'     => $pages[0]['title'],
				'data'     => $pages[0],
				'class'    => $pages[$i]['cssClass']
			);

			list($strSection, $strArticle) = explode(':', \Input::get('articles'));

			if ($strArticle === null)
			{
				$strArticle = $strSection;
			}

			$objArticle = \ArticleModel::findByIdOrAlias($strArticle);
			$strAlias = ($objArticle->alias != '' && !\Config::get('disableAlias')) ? $objArticle->alias : $objArticle->id;

			if ($objArticle->inColumn != 'main')
			{
				$strAlias = $objArticle->inColumn . ':' . $strAlias;
			}

			if ($objArticle !== null)
			{
				$items[] = array
				(
					'isRoot'   => false,
					'isActive' => true,
					'href'     => $this->generateFrontendUrl($pages[0], '/articles/' . $strAlias),
					'title'    => specialchars($objArticle->title, true),
					'link'     => $objArticle->title,
					'data'     => $objArticle->row(),
					'class'    => ''
				);
			}
		}

		// Active page
		else
		{
			// Set href for regular pages only
			if($pages[0]['type'] == 'regular')
			{
				$strUrl = $this->generateFrontendUrl($pages[0]);
			}
			else
			{
				$strUrl = '';
			}

			$items[] = array
			(
				'isRoot'   => false,
				'isActive' => true,
				'href'     => $strUrl,
				'title'    => specialchars($pages[0]['pageTitle'] ?: $pages[0]['title']),
				'link'     => $pages[0]['title'],
				'data'     => $pages[0],
				'class'    => ''
			);
		}

		// Mark the first element (see #4833)
		$items[0]['class'] = 'first';

		// HOOK: add custom logic
		if (isset($GLOBALS['TL_HOOKS']['generateBreadcrumb']) && is_array($GLOBALS['TL_HOOKS']['generateBreadcrumb']))
		{
			foreach ($GLOBALS['TL_HOOKS']['generateBreadcrumb'] as $callback)
			{
				$this->import($callback[0]);
				$items = $this->{$callback[0]}->{$callback[1]}($items, $this);
			}
		}

		// Get Schema.org data - check if Contao is higher than 4.13
		if (version_compare(VERSION . '.' . BUILD, '4.13.0', '>='))
		{
			$this->Template->getSchemaOrgData = static function () use ($items): array
			{
				$jsonLd = array
				(
					'@type' => 'BreadcrumbList',
					'itemListElement' => array()
				);

				$position = 0;
				$htmlDecoder = \System::getContainer()->get('contao.string.html_decoder');

				foreach ($items as $item)
				{
					$jsonLd['itemListElement'][] = array
					(
						'@type' => 'ListItem',
						'position' => ++$position,
						'item' => array(
							'@id' => $item['href'] ?: './',
							'name' => $htmlDecoder->inputEncodedToPlainText($item['link'])
						)
					);
				}

				return $jsonLd;
			};
		}

		$this->Template->items = $items;
	}
}
