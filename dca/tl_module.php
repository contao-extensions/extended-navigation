<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-extended-navigation
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */

$strTable = 'tl_module';

/**
 * Extend default palettes
 */
$GLOBALS['TL_DCA'][$strTable]['palettes']['navigation'] = str_replace(',showProtected', ',showProtected,hidePages', $GLOBALS['TL_DCA'][$strTable]['palettes']['navigation']);
$GLOBALS['TL_DCA'][$strTable]['palettes']['breadcrumb'] = str_replace(',showHidden', ',showHidden,hidePages', $GLOBALS['TL_DCA'][$strTable]['palettes']['breadcrumb']);

/**
 * Add fields to tl_module
 */
$GLOBALS['TL_DCA'][$strTable]['fields']['hidePages'] = array
(
	'label'							=> &$GLOBALS['TL_LANG'][$strTable]['hidePages'],
	'exclude'						=> true,
	'inputType'					=> 'pageTree',
	'eval'							=> array('multiple'=>true, 'tl_class'=>'clr', 'fieldType'=>'checkbox'),
	'sql'								=> "blob NULL"
);
