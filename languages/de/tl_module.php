<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-extended-navigation
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */

$strTable = 'tl_module';

/**
 * Fields
 */
$GLOBALS['TL_LANG'][$strTable]['hidePages'] = array('Seiten ignorieren', 'Sie können hier einstellen, dass diese Seite nicht im Modul angezeigt werden sollen. Wenn Sie diese Einstellung wählen und die Option "Unterseiten einbeziehen" nicht auswählen, müssen Sie ihr beim Navigationsmodul das mitgelieferte nav_hidepages Template auswählen oder Ihr eigenes nav_ Template entsprechend anpassen.');
