<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-extended-navigation
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */


/**
 * Content elements
 */
$GLOBALS['FE_MOD']['navigationMenu']['navigation'] = '\Navigation\ModuleNavigationExt';
$GLOBALS['FE_MOD']['navigationMenu']['breadcrumb'] = '\Navigation\ModuleBreadcrumbExt';
