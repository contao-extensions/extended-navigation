<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-extended-navigation
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Modules
	'Navigation\ModuleNavigationExt' => 'system/modules/extended-navigation/modules/ModuleNavigationExt.php',
	'Navigation\ModuleBreadcrumbExt' => 'system/modules/extended-navigation/modules/ModuleBreadcrumbExt.php',
));


/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'nav_hidepages' => 'system/modules/extended-navigation/templates/navigation',
));
